﻿using System;
using System.Linq;
using Cwc.Core.Models;
using Cwc.Database;

namespace Cwc.Core.Services
{
    public class ContinentService
    {
        public static IQueryable<ContinentViewModel> GetAllContinents()
        {
            var continentRepository = new Repository<Continent>();

            var continentModel = continentRepository.GetAll()
                .Select(continent =>
                    new ContinentViewModel
                    {
                        Id = continent.Id,
                        Name = continent.Name,
                        Code = continent.Code,
                    });

            return continentModel;
        }

        public static ContinentViewModel GetContinent(int continentId)
        {
            var continent = GetAllContinents().FirstOrDefault(c => c.Id == continentId);
            return continent;
        }

        public static ContinentViewModel GetContinentByCode(string code)
        {
            var continent = GetAllContinents().FirstOrDefault(c => c.Code == code);
            return continent;
        }

        public static ContinentViewModel GetContinentByName(string continentName)
        {
            var continent = GetAllContinents().FirstOrDefault(c => c.Name == continentName);
            return continent;
        }

        public static void AddContinent(ContinentViewModel continentViewModel)
        {
            using (var ctx = new CWCEntities())
            {
                var continent = new Continent()
                {
                    Name = continentViewModel.Name,
                    Code = continentViewModel.Code
                };
                ctx.Continents.Add(continent);
                ctx.SaveChanges();
            }
        }

        public static void EditContinent(int id, ContinentViewModel continentViewModel)
        {
            using (var ctx = new CWCEntities())
            {
                var continent = ctx.Continents.Find(id);
                if (continent != null)
                {
                    continent.Name = continentViewModel.Name;
                    continent.Code = continentViewModel.Code;
                }
                ctx.SaveChanges();
            }
        }

        public static void DeleteContinent(int continentId)
        {
            using (var ctx = new CWCEntities())
            {
                var continent = ctx.Continents.Find(continentId);
                if (continent != null)
                {
                    var countryRepository = new Repository<Country>();
                    var countries = countryRepository.Where( c => c.ContinentId == continentId);
                    foreach (var country in countries)
                    {
                        countryRepository.Remove(country);
                        countryRepository.Save();
                    }
                    
                    ctx.Continents.Remove(continent);
                    ctx.SaveChanges();
                }
            }
        }
    }
}
