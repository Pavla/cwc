import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogBoxContinentComponent } from './../dialog-box-continent/dialog-box-continent.component';
import { CrudService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-continents',
  templateUrl: './continents.component.html',
  styleUrls: ['./continents.component.css']
})
export class ContinentsComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'code', 'action'];
  dataSource = [];
  constructor(public dialog: MatDialog, public crudService: CrudService) { }

  ngOnInit(): void {

    this.fetchContinents();

  }

  fetchContinents(): any {
    return this.crudService.getContinentData().subscribe((response: {}) => {
      this.dataSource = JSON.parse(JSON.stringify(response));
    });
  }

  openDialog(action: any, obj: any): any {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxContinentComponent, {
      width: '450px',
      data: obj
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event !== 'Cancel') {
        this.fetchContinents();
      }
    });
  }

}
