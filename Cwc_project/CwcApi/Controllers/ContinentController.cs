﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cwc.Core.Models;
using Cwc.Core.Services;
using System;
using System.Net.Http;
using System.Net;

namespace CwcApi.Controllers
{
    public class ContinentController : ApiController
    {
        // GET: api/Continent
        public IEnumerable<ContinentViewModel> GetContinents()
        {
            try
            {
                var continents = ContinentService.GetAllContinents()
                    .OrderBy(c => c.Name);

                return continents;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        // GET: api/Continent/5
        public HttpResponseMessage GetContinent(int id)
        {
            try
            {
                var continent = ContinentService.GetContinent(id);
                if (continent == null)
                {
                    HttpError err = new HttpError(string.Format("Continent not found"));
                    return Request.CreateResponse(HttpStatusCode.NotFound, err);
                }
                return Request.CreateResponse(HttpStatusCode.OK, continent);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        // POST: api/Continent
        public HttpResponseMessage Post([FromBody]ContinentViewModel newContinent)
        {
            try
            {
                var continent = ContinentService.GetContinentByCode(newContinent.Code);
                if (continent != null)
                {
                    HttpError err = new HttpError(string.Format("Continent {0} already exist", newContinent.Name));
                    return Request.CreateResponse(HttpStatusCode.Conflict, err);
                }
                ContinentService.AddContinent(newContinent);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // PUT: api/Continent/5
        public HttpResponseMessage Put(int id, [FromBody]ContinentViewModel editContinent)
        {
            try
            {
                ContinentService.EditContinent(id, editContinent);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // DELETE: api/Continent/5
        public void Delete(int id)
        {
            try
            {
                ContinentService.DeleteContinent(id);
            }
            catch (Exception ex)
            {
                throw ex;
            } 
        }
    }
}
