import { Component, OnInit } from '@angular/core';
import { MatTableDataSource  } from '@angular/material/table';
import { animate, state, style, transition, trigger } from '@angular/animations';

import { continents, countries } from 'countries-list';
import { Helper } from '../../util/helper';
import { CountryData } from '../../model/country-data';
import { ContinentData } from '../../model/continent-data';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AllComponent implements OnInit {

  isTableExpanded = false;

  continentsList: { [key: string]: string } = continents;
  countriesList = countries;
  countriesArray: CountryData[] = [];
  continentsWithCountriesArray: ContinentData[] = [];

  // dataContinentList = new MatTableDataSource();
  dataContinentList = new MatTableDataSource<ContinentData>(this.continentsWithCountriesArray);
  displayedContinentColumnsList: string[] = ['id', 'continent', 'code', 'actions'];

  constructor() { }

  ngOnInit(): void {

    for (const [countryCode, country] of Object.entries(this.countriesList)) {
      this.countriesArray.push({
        continentName: this.continentsList[country.continent],
        name: country.name,
        code: countryCode,
        phone: country.phone,
        capital: country.capital,
        currency: country.currency,
        languages: country.languages.toString()
      });
    }

    const continentsGroup: ContinentData[] = Helper.groupBy(this.countriesArray, 'continentName');
    for (const [continentName, countriesList] of Object.entries(continentsGroup)) {
      this.continentsWithCountriesArray.push({
        name: continentName,
        code: Object.keys(this.continentsList).find(key => this.continentsList[key] === continentName),
        countries: countriesList as unknown as CountryData[]
      });
    }

    this.dataContinentList.data = this.continentsWithCountriesArray;
  }

  toggleTableRows(): void {
    this.isTableExpanded = !this.isTableExpanded;

    this.dataContinentList.data.forEach((row: any) => {
      row.isExpanded = this.isTableExpanded;
    });
  }

  findCountry(event: any): void {
      const searchText = event.target.value;
      if (searchText) {
        const filteredContinentsWithCountries = JSON.parse(JSON.stringify(this.continentsWithCountriesArray))
         .filter((c: { countries: any[]; }) => c.countries.some((ct: { name: string; }) => ct.name.toLocaleLowerCase()
            .startsWith(searchText.toLocaleLowerCase())))
         .map((obj: { countries: any[]; }) => {
            obj.countries = obj.countries.filter((ct: { name: string; }) =>
            ct.name.toLocaleLowerCase().startsWith(searchText.toLocaleLowerCase()));
            return obj;
          });

        this.dataContinentList.data = filteredContinentsWithCountries;
    } else {
      this.dataContinentList.data = this.continentsWithCountriesArray;
    }
  }
}
