import { Injectable } from '@angular/core';
import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { CountryData } from './../model/country-data';
import { Continent, ContinentData } from './../model/continent-data';

@Injectable({
  providedIn: 'root'
})

export class CrudService {

  // REST API
  endpoint = 'http://localhost:64124/api';

  constructor(private httpClient: HttpClient) { }

  httpHeader = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getContinentData(): Observable<ContinentData> {
    return this.httpClient.get<ContinentData>(this.endpoint + '/continent')
    .pipe(
      catchError(this.processError)
    );
  }

  getSingleContinentData(id: string): Observable<ContinentData> {
    return this.httpClient.get<ContinentData>(this.endpoint + '/continent/' + id)
    .pipe(
      catchError(this.processError)
    );
  }

  addContinentData(data: any): Observable<Continent> {
    return this.httpClient.post<Continent>(this.endpoint + '/continent', JSON.stringify(data), this.httpHeader)
    .pipe(
      catchError(this.processError)
    );
  }

  updateContinentData(id: number, data: any): Observable<Continent> {
    return this.httpClient.put<Continent>(this.endpoint + '/continent/' + id, JSON.stringify(data), this.httpHeader)
    .pipe(
      catchError(this.processError)
    );
  }

  deleteContinentData(id: number): any{
    return this.httpClient.delete<Continent>(this.endpoint + '/continent/' + id, this.httpHeader)
    .pipe(
      catchError(this.processError)
    );
  }

  getCountryData(): Observable<CountryData> {
    return this.httpClient.get<CountryData>(this.endpoint + '/country')
    .pipe(
      catchError(this.processError)
    );
  }

  getSingleCountryData(id: string): Observable<CountryData> {
    return this.httpClient.get<CountryData>(this.endpoint + '/country/' + id)
    .pipe(
      catchError(this.processError)
    );
  }

  addCountryData(data: any): Observable<CountryData> {
    return this.httpClient.post<CountryData>(this.endpoint + '/country', JSON.stringify(data), this.httpHeader)
    .pipe(
      catchError(this.processError)
    );
  }

  updateCountryData(id: number, data: any): Observable<CountryData> {
    return this.httpClient.put<CountryData>(this.endpoint + '/country/' + id, JSON.stringify(data), this.httpHeader)
    .pipe(
      catchError(this.processError)
    );
  }

  // tslint:disable-next-line:typedef
  deleteCountryData(id: number){
    return this.httpClient.delete<CountryData>(this.endpoint + '/country/' + id, this.httpHeader)
    .pipe(
      catchError(this.processError)
    );
  }

  // tslint:disable-next-line:typedef
  processError(err: { error: { message: string; }; status: any; message: any; }) {
    return throwError(err);
  }
}
