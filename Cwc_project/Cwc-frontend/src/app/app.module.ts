import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';

import { CountriesComponent } from './component/countries/countries.component';
import { ContinentsComponent } from './component/continents/continents.component';
import { PageNotFoundComponent } from './component/page-not-found/page-not-found.component';
import { AllComponent } from './component/all/all.component';
import { DialogBoxContinentComponent } from './component/dialog-box-continent/dialog-box-continent.component';
import { DialogBoxCountryComponent } from './component/dialog-box-country/dialog-box-country.component';

@NgModule({
  declarations: [
    AppComponent,
    CountriesComponent,
    ContinentsComponent,
    PageNotFoundComponent,
    AllComponent,
    DialogBoxContinentComponent,
    DialogBoxCountryComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MatTableModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatInputModule,
    MatListModule,
    MatSelectModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
