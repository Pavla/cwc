﻿using System;
using System.Net.Http;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cwc.Core.Models;
using Cwc.Core.Services;

namespace CwcApi.Controllers
{
    public class CountryController : ApiController
    {
        // GET: api/Country
        public IEnumerable<CountryViewModel> GetCountries()
        {
            try
            {
                var countries = CountryService.GetAllCountries()
                .OrderBy(c => c.Name);

                return countries;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // GET: api/Country/5
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var country = CountryService.GetCountry(id);
                if (country == null)
                {
                    HttpError err = new HttpError(string.Format("Country not found"));
                    return Request.CreateResponse(HttpStatusCode.NotFound, err);
                }
                return Request.CreateResponse(HttpStatusCode.OK, country);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // POST: api/Country
        public HttpResponseMessage Post(CountryViewModel newCountry)
        {
            try
            {
                var continent = ContinentService.GetContinentByName(newCountry.ContinentName);
                if (continent == null)
                {
                    HttpError err = new HttpError(string.Format("Continent not exist. Please create continent before add new country"));
                    return Request.CreateResponse(HttpStatusCode.Conflict, err);
                }

                var country = CountryService.GetCountryByCode(newCountry.Code);
                if (country != null)
                {
                    HttpError err = new HttpError(string.Format("Country {0} already exist", newCountry.Name));
                    return Request.CreateResponse(HttpStatusCode.Conflict, err);
                }
                CountryService.AddCountry(newCountry);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // PUT: api/Country/5
        public HttpResponseMessage Put(int id, [FromBody]CountryViewModel editCountry)
        {
            try
            {
                CountryService.EditCountry(id, editCountry);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // DELETE: api/Country/5
        public void Delete(int id)
        {
            try
            {
                CountryService.DeleteCountry(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
