﻿using System;
using System.Linq;
using Cwc.Core.Models;
using Cwc.Database;

namespace Cwc.Core.Services
{
    public class CountryService
    {
        public static IQueryable<CountryViewModel> GetAllCountries()
        {
            var countryRepository = new Repository<Country>();

            var countryModel = countryRepository.GetAll()
                .Select(country =>
                    new CountryViewModel
                    {
                        Id = country.Id,
                        Name = country.Name,
                        Code = country.Code,
                        ContinentId = country.ContinentId,
                        ContinentName = country.Continent.Name,
                        Phone = country.Phone,
                        Capital = country.Capital,
                        Currency = country.Currency,
                        Languages = country.Languages
                    });

            return countryModel;
        }

        public static CountryViewModel GetCountry(int countryId)
        {
            var country = GetAllCountries().FirstOrDefault(c => c.Id == countryId);
            return country;
        }

        public static CountryViewModel GetCountryByCode(string code)
        {
            var country = GetAllCountries().FirstOrDefault(c => c.Code == code);
            return country;
        }

        public static IQueryable<CountryViewModel> GetCountries(int continentID)
        {
            var country = GetAllCountries().Where(c => c.ContinentId == continentID);
            return country;
        }

        public static void AddCountry(CountryViewModel countryViewModel)
        {
            using (var ctx = new CWCEntities())
            {
                var country = new Country()
                {
                    Name = countryViewModel.Name,
                    Code = countryViewModel.Code,
                    ContinentId = ContinentService.GetContinentByName(countryViewModel.ContinentName).Id,
                    Phone = countryViewModel.Phone,
                    Capital = countryViewModel.Capital,
                    Currency = countryViewModel.Currency,
                    Languages = countryViewModel.Languages
                };
                ctx.Countries.Add(country);
                ctx.SaveChanges();
            }
        }

        public static void EditCountry(int id, CountryViewModel countryViewModel)
        {
            using (var ctx = new CWCEntities())
            {
                var country = ctx.Countries.Find(id);
                if (country != null)
                {
                    country.Name = countryViewModel.Name;
                    country.Code = countryViewModel.Code;
                    country.ContinentId = ContinentService.GetContinentByName(countryViewModel.ContinentName).Id;
                    country.Phone = countryViewModel.Phone;
                    country.Capital = countryViewModel.Capital;
                    country.Currency = countryViewModel.Currency;
                    country.Languages = countryViewModel.Languages;
                }
                ctx.SaveChanges();
            }
        }

        public static void DeleteCountry(int countryId)
        {
            using (var ctx = new CWCEntities())
            {
                var country = ctx.Countries.Find(countryId);
                if (country != null)
                {
                    ctx.Countries.Remove(country);
                    ctx.SaveChanges();
                }
            }
        }
    }
}
