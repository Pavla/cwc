import { Component, OnInit, Inject, Optional, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';

import { continents } from 'countries-list';
import { Continent } from '../../model/continent-data';
import { CrudService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-dialog-box-continent',
  templateUrl: './dialog-box-continent.component.html',
  styleUrls: ['./dialog-box-continent.component.css']
})
export class DialogBoxContinentComponent implements OnInit {

  action: string;
  localData: any;
  errorMessage: string;

  continents: Continent[] = [];
  continentsList: { [key: string]: string } = continents;
  dropdownContinents: Continent[] = [];

  @ViewChild(MatTable, { static: true }) table: MatTable<any> | undefined;

  constructor(
    public dialogRef: MatDialogRef<DialogBoxContinentComponent>,
    public crudService: CrudService,
    // @Optional() is used to prevent error if no data is passed
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Continent) {
    this.localData = { ...data };
    this.action = this.localData.action;
  }

  ngOnInit(): void {

    for (const [continentCode] of Object.entries(this.continentsList)) {
      this.continents.push({
        name: this.continentsList[continentCode],
        code: continentCode
      });
    }
    this.dropdownContinents = JSON.parse(JSON.stringify(this.continents));
  }

  onContinentChange(event: any): void {
    const dropDownSelected = this.continents.find((data: any) => data.name === event.value);
    this.localData.code = dropDownSelected?.code;
  }

  doAction(): void {

    if (this.action === 'Add') {
      this.addRowData(this.localData);
    } else if (this.action === 'Delete') {
      this.deleteRowData(this.localData);
    }
  }

  closeDialog(): void {
    this.dialogRef.close({ event: 'Cancel' });
  }

  addRowData(rowObj: any): any {
    const continent = new Continent();
    continent.name = rowObj.name;
    continent.code = rowObj.code;

    this.crudService.addContinentData(continent).subscribe(() => {
      this.errorMessage = '';
      this.dialogRef.close({ event: this.action, data: this.localData });
    },
      error => {
        this.errorMessage = error.error.message;
      });
  }

  deleteRowData(rowObj: { id: number; }): any {
    this.crudService.deleteContinentData(rowObj.id).subscribe(() => {
      this.errorMessage = '';
      this.dialogRef.close({ event: this.action, data: this.localData });
    },
      (error: { error: { message: string; }; }) => {
        this.errorMessage = error.error.message;
      });
  }

}
