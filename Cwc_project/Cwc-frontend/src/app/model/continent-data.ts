import { Observable } from 'rxjs';
import { CountryData } from './../model/country-data';

export class Continent {
    name: string;
    code?: string;
}

export class ContinentData extends Continent {
    countries: CountryData[];
}
