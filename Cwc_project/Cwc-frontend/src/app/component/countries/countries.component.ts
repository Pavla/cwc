import { Component, OnInit, ViewChild } from '@angular/core';

import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { DialogBoxCountryComponent } from './../dialog-box-country/dialog-box-country.component';

import { CountryData } from '../../model/country-data';
import { CrudService } from 'src/app/services/crud.service';
import { Continent, ContinentData } from 'src/app/model/continent-data';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent implements OnInit {

  continentsDb: Continent[];

  displayedColumns: string[] = ['id', 'name', 'continent', 'capital', 'phone', 'currency', 'languages', 'action'];
  dataSource: Array<CountryData> = [];

  @ViewChild(MatTable, { static: true }) table: MatTable<any> | undefined;

  constructor(public dialog: MatDialog, public crudService: CrudService) { }

  ngOnInit(): void {

    this.fetchCountries();

    this.fetchContinents();

  }

  fetchContinents(): any {
    return this.crudService.getContinentData().subscribe((response: {}) => {
      const continentArray = (JSON.parse(JSON.stringify(response)));
      this.continentsDb = continentArray.map((c: { name: any; code: any; }) => {
        return ({ name: c.name, code: c.code });
      });
    });
  }

  fetchCountries(): any {
    return this.crudService.getCountryData().subscribe((response: {}) => {
      this.dataSource = JSON.parse(JSON.stringify(response));
    });
  }

  openDialog(action: any, obj: any): any {
    obj.action = action;
    obj.continentsDb = this.continentsDb;
    const dialogRef = this.dialog.open(DialogBoxCountryComponent, {
      width: '450px',
      data: obj
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event !== 'Cancel') {
        this.fetchCountries();
      }
    });
  }
}
