import { Component, OnInit, Inject, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { continents, countries } from 'countries-list';
import { CountryData } from '../../model/country-data';
import { Continent } from '../../model/continent-data';
import { CrudService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-dialog-box-country',
  templateUrl: './dialog-box-country.component.html',
  styleUrls: ['./dialog-box-country.component.css']
})
export class DialogBoxCountryComponent implements OnInit {

  action: string;
  localData: any;
  continentsDb: Continent[];
  errorMessage: string;

  continentsList: { [key: string]: string } = continents;
  countriesList = countries;
  countries: CountryData[] = [];
  dropdownCountries: CountryData[] = [];

  constructor(
    public dialogRef: MatDialogRef<DialogBoxCountryComponent>,
    public crudService: CrudService,
    // @Optional() is used to prevent error if no data is passed
    @Optional() @Inject(MAT_DIALOG_DATA) public dataCountry: CountryData) {
    this.localData = { ...dataCountry };
    this.action = this.localData.action;
    this.continentsDb = this.localData.continentsDb;
  }

  ngOnInit(): void {

    for (const [countryCode, country] of Object.entries(this.countriesList)) {
      this.countries.push({
        continentName: this.continentsList[country.continent],
        name: country.name,
        code: countryCode,
        phone: country.phone,
        capital: country.capital,
        currency: country.currency,
        languages: country.languages.toString()
      });
    }

    this.dropdownCountries = this.countries;
  }

  doAction(): void {

    if (this.action === 'Add') {
      this.addRowData(this.localData);
    }
    else if (this.action === 'Update') {
      this.updateRowData(this.localData);
    }
    else if (this.action === 'Delete') {
      this.deleteRowData(this.localData);
    }
  }
  closeDialog(): void {
    this.dialogRef.close({ event: 'Cancel' });
  }

  onContinentChange(event: any): void {
    this.dropdownCountries = this.countries.filter(c => c.continentName === event.value);

    this.localData.code = '';
    this.localData.phone = '';
    this.localData.capital = '';
    this.localData.currency = '';
    this.localData.languages = '';
  }

  onCountryChange(event: any): void {
    const dropDownSelected = this.countries.find(c => c.name === event.value);
    this.localData.code = dropDownSelected?.code;
    this.localData.phone = dropDownSelected?.phone;
    this.localData.capital = dropDownSelected?.capital;
    this.localData.currency = dropDownSelected?.currency;
    this.localData.languages = dropDownSelected?.languages;
  }

  addRowData(rowObj: any): any {
    const country = new CountryData();
    country.name = rowObj.name;
    country.code = rowObj.code;
    country.phone = rowObj.phone;
    country.continentName = rowObj.continentName;
    country.capital = rowObj.capital;
    country.currency = rowObj.currency;
    country.languages = rowObj.languages;

    this.crudService.addCountryData(country).subscribe(() => {
      this.errorMessage = '';
      this.dialogRef.close({ event: this.action, data: this.localData });
    },
      error => {
        this.errorMessage = error.error.message;
      });
  }

  updateRowData(rowObj: any): any {
    const country = new CountryData();
    country.name = rowObj.name;
    country.code = rowObj.code;
    country.phone = rowObj.phone;
    country.continentName = rowObj.continentName;
    country.capital = rowObj.capital;
    country.currency = rowObj.currency;
    country.languages = rowObj.languages;

    this.crudService.updateCountryData(rowObj.id, country).subscribe(() => {
      this.errorMessage = '';
      this.dialogRef.close({ event: this.action, data: this.localData });
    },
      error => {
        this.errorMessage = error.error.message;
      });
  }

  deleteRowData(rowObj: { id: number; }): any {
    this.crudService.deleteCountryData(rowObj.id).subscribe(result => {
      this.errorMessage = '';
      this.dialogRef.close({ event: this.action, data: this.localData });
    },
      error => {
        this.errorMessage = error.error.message;
      });
  }

}
