﻿namespace Cwc.Core.Models
{
    public class CountryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int ContinentId { get; set; }
        public string ContinentName { get; set; }
        public string Phone { get; set; }
        public string Capital { get; set; }
        public string Currency { get; set; }
        public string Languages { get; set; }

    }
}
