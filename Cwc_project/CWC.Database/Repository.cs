﻿using System;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace Cwc.Database
{
    public class Context : CWCEntities { }

    public class Repository<T> where T : class
    {
        private Context _context = new Context();

        public Context Context
        {
            get { return _context; }
            set { _context = value; }
        }

        public virtual IQueryable<T> GetAll()
        {
            var query = _context.Set<T>();
            return query;
        }

        public IQueryable<T> Where(Expression<Func<T, bool>> predicate)
        {
            var query = _context.Set<T>().Where(predicate);
            return query;
        }

        public virtual void Add(T entity)
        {
            _context.Set<T>().Add(entity);
        }

        public virtual void Remove(T entity)
        {
            _context.Set<T>().Remove(entity);
        }

        public virtual void Modified(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Save()
        {
            _context.SaveChanges();
        }
    }
}
