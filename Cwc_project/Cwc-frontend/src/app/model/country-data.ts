export class CountryData {
    name: string;
    code: string;
    phone: string;
    continentName: string;
    capital: string;
    currency: string;
    languages: string;
}
